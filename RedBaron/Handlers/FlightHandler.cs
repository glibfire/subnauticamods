﻿using MoreCyclopsUpgrades.API;
using MoreCyclopsUpgrades.API.General;
using MoreCyclopsUpgrades.API.Upgrades;
using System;
using UnityEngine;
using static RedBaron.MainPatcher;

namespace RedBaron {
    internal class FlightHandler : UpgradeHandler {

        private readonly CyclopsFlightModule flightModule;
        private GameObject airshipPrefab;

        public FlightHandler(CyclopsFlightModule cyclopsFlightModule, SubRoot cyclops) : base(CyclopsFlightModule.TechTypeID, cyclops)
        {
            flightModule = cyclopsFlightModule;

            OnFinishedUpgrades = () =>
            {
                //if (RedBaronConfig.enableExtraAssets)
                //{
                //    var path = @"./QMods/RedBaron/Assets/redbaron";
                //    if (assetBundle == null)
                //    {
                //        assetBundle = AssetBundle.LoadFromFile(path);
                //    }
                //    if(assetBundle == null)
                //    {
                //        Console.WriteLine("ERROR: RedBaron: Cannot locate asset bundle.");
                //    }

                //// Enable the balloon if allows by config
                
                //    GameObject prefab = assetBundle.LoadAsset<GameObject>("Low-Poly_airship");
                //    if (prefab == null)
                //    {
                //        Console.WriteLine("ERROR: RedBaron: Cannot locate airship prefab");
                //    }
                //    else
                //    {
                //        GameObject airship = GameObject.Instantiate(prefab, cyclops.transform.position, Quaternion.identity);
                //        airship.transform.SetParent(cyclops.transform, false);
                //        airship.transform.position = new Vector3(airship.transform.position.x, airship.transform.position.y + 11f, airship.transform.position.z);
                //        airship.transform.localScale = new Vector3(10f, 10f, 10f);
                //        Shader shader = Shader.Find("MarmosetUBER");
                //        Renderer[] renderers = airship.GetComponentsInChildren<Renderer>();
                //        SkyApplier skyApplier = airship.AddComponent<SkyApplier>();
                //        skyApplier.renderers = renderers;
                //        skyApplier.anchorSky = Skies.Auto;
                //        airship.SetActive(false);
                //        airshipPrefab = airship;

                //    }
                //}

                //if(MCUServices.CrossMod.HasUpgradeInstalled(cyclops, CyclopsFlightModule.TechTypeID))
                //{
                //    if(RedBaronConfig.enableExtraAssets) {
                //        airshipPrefab.SetActive(true);
                //    }
                //}
                //else
                //{
                //    if (RedBaronConfig.enableExtraAssets)
                //    {
                //        airshipPrefab.SetActive(false);
                //    }
                //}
            };

        }

    }
}
