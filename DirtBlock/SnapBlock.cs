﻿using DirtBlock.Controllers;
using SMLHelper.V2.Assets;
using SMLHelper.V2.Crafting;
using SMLHelper.V2.Handlers;
using SMLHelper.V2.Utility;
using System;
using UnityEngine;
using UnityEngine.UI;

namespace DirtBlock {
    internal class SnapBlock : Buildable {
        private const string NameID = "SnapBlock";

        public CraftTree.Type TreeTypeID { get; private set; }
        public override TechGroup GroupForPDA { get; } = TechGroup.ExteriorModules;
        public override TechCategory CategoryForPDA { get; } = TechCategory.ExteriorModule;
        public override string AssetsFolder { get; } = "DirtBlock/Assets";

        private GameObject _Prefab;

        internal SnapBlock()
            : base(NameID,
                  "Snap Block",
                  "A strangely cubical and low res block that snaps to others of its type.")
        {
            OnStartedPatching += () =>
            {
                Console.WriteLine("DirtBlock: Beginning to patch SnapBlock");
            };

            OnFinishedPatching += () =>
            {
                Console.WriteLine("DirtBlock: Finishing patching SnapBlock");
            };
        }

        protected override TechData GetBlueprintRecipe()
        {
            return new TechData()
            {
                craftAmount = 1,
                Ingredients =
                {
                    new Ingredient(TechType.Titanium, 1),
                }
            };
        }

        private bool GetPrefabs()
        {

            GameObject prefab = MainPatcher.assetBundle.LoadAsset<GameObject>("snapblock");
            if (prefab != null)
            {
                _Prefab = prefab;
                return true;
            }

            Console.WriteLine("ERROR: DirtBlock: Prefab not found, SnapBlock");
            return false;
        }

        public override GameObject GetGameObject()
        {
            GameObject prefab = null;

            if (!GetPrefabs()) return null;

            try
            {
                prefab = GameObject.Instantiate(_Prefab);
                //========== Allows the building animation and material colors ==========// 
                Shader shader = Shader.Find("MarmosetUBER");
                Renderer[] renderers = prefab.GetComponentsInChildren<Renderer>();
                SkyApplier skyApplier = prefab.AddComponent<SkyApplier>();
                skyApplier.renderers = renderers;
                skyApplier.anchorSky = Skies.Auto;

                //========== Allows the building animation and material colors ==========// 

                // Add constructible
                var constructable = prefab.AddComponent<Constructable>();
                constructable.allowedOnWall = true;
                constructable.allowedOnGround = true;
                constructable.allowedInSub = false;
                constructable.allowedInBase = false;
                constructable.allowedOnCeiling = true;
                constructable.allowedOutside = true;
                constructable.model = prefab.FindChild("model");
                constructable.techType = TechType;
                constructable.rotationEnabled = true;
                constructable.allowedOnConstructables = true;

                // Add large world entity ALLOWS YOU TO SAVE ON TERRAIN
                var lwe = prefab.AddComponent<LargeWorldEntity>();
                lwe.cellLevel = LargeWorldEntity.CellLevel.Near;

                prefab.AddComponent<PrefabIdentifier>().ClassId = this.ClassID;
                prefab.AddComponent<TechTag>().type = TechType;
                DirtBlockController controller = prefab.AddComponent<DirtBlockController>();

                if (controller == null)
                {
                }


            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            return prefab;
        }

    }
}

