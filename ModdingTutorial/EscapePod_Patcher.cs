﻿using Harmony;
using System;
using UnityEngine;

namespace Unleashed_Lifepod   // This is usually going to be the name of your mod. Just make it consistent for each file in a specific mod.
{

    [HarmonyPatch(typeof(EscapePod))]   // We're going to patch the EscapePod class.
    [HarmonyPatch("StartAtPosition")]   // The 'StartAtPosition' method of the EscapePod class.
    internal class EscapePod_StartAtPosition_Patch
    {
        [HarmonyPrefix]                 // Run our patch before the game's default code.
        public static bool Prefix(ref Vector3 position,  EscapePod __instance)   // EscapePod __instance gives us the equivalent of 'this.'
        {

            __instance.transform.position = position;  
            __instance.anchorPosition = position;
            __instance.RespawnPlayer();   // So if you compare this with the original EscapePod.StartAtPosition method using dnSpy, you'll see this line as this.RespawnPlayer()
            return false;               // returning false means the original method with Subnautica will not run. We replace it entirely with this one.
        }

    }

}
