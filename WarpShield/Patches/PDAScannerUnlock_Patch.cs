﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Harmony;

namespace MAC.WarpShield {
    [HarmonyPatch(typeof(PDAScanner))]
    [HarmonyPatch("Unlock")]
    public static class PDAScannerUnlockPatch {
        public static bool Prefix(PDAScanner.EntryData entryData)
        {
            if(entryData.key == TechType.Warper)
            {
                if (!KnownTech.Contains(WarpShieldModule.TechTypeID))
                {
                    KnownTech.Add(WarpShieldModule.TechTypeID);
                    ErrorMessage.AddMessage("Added blueprint for phasic stabilizer to database");
                }
            }
            return true;
        }
    }
}
